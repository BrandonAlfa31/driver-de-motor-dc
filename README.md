# Proyecto Driver de motor DC 

## Descripción
Este es un primer acercamiento al mundo del diseño de PCBs, para esta primera experiencia se desea crear un controlador, o "driver" de un motor DC, el cual podrá controlar más de un motor a la vez y podrá soportar tensiones desde los 5V hasta los 12V y una corriente pico de 2A. La particularidad de este controlador es que si se quiere variar la velocidad del motor mediante PWM, se podrá hacer mediante el control a distancia, esto se logrará usando un receptor infrarrojo situado en la PCB y que recibirá las órdenes mediante un control remoto de televisión. Cabe destacar que es un proyecto que lleverá parte de programación (el uso de arduino), al igual que algunos detalles para hacer un controlador distinto (más personalizado).

## Usos
El uso que se le puede dar a la PCB es muy variado, ya de entrada es para poder hacer funcionar un motor DC, luego de esto, las posibilidades son muchas a nivel de proyectos electrónicos desde muy básicos hasta muy elaborados, esto dependería de la creatividad de la persona, pero se presta para muchos proyectos diversos, uno podría ser en controlar la ruedas de un carro, donde este pueda ir hacia delante o hacia atras, algo básico; también, se podría usar en un sistema de mayor grado de libertad donde las ruedas puedan girar, pero ya esto es considerando un siguiente nivel.

## Tipo de instalación
Dado que la placa se controla mediante señales que pasan por un Arduino, esta se debe montar sobre la placa del Arduino UNO, mediante conectores de pines (hembra en el Arduino y macho en la placa a diseñar).

## Soporte
brad.cr7@gmail.com

## Mapa de la ruta a seguir
Se diseñó el esquemático del circuito a elaborar en la PCB, así como la asignación de todos los parámetros de los componentes. Una vez verificado todo con respecto al espemático se procedió a montar el circuito en la placa, así como la definición de la forma del contorno de la placa. Con el trabajo del esquemático y el diseño de la placa, se mandó a imprimir la placa y a comprar los componentes para posteriormente proceder a soldar y verificar el funcionamiento de la PCB. 

### Implementaciones Futuras:
  * Implementación Futura 1: Poder controlar la velocidad de un motor DC de 12V dc mediante una orden a distancia usando infrarrojo.
  * Implementación Futura 2: Poder usar el controlador para un uso personal en un proyecto de mayor complejidad.

## Autores y reconocimiento
  * Brandon Palacio Delgado (brad.cr7@gmail.com)
  * Randolph Villegas Rodríguez (randolphvillegas1106@gmail.com)

## Fabricante
La fabricación de la PCB se realizará mediante la compañía de fabricación de PCBs: JLCPCB.
Las capacidades de fabricación de este fabricanete se encuentran en el siguiente enlace: https://jlcpcb.com/capabilities/pcb-capabilities

## Licencia
Este proyecto trabaja bajo la licencia GPLv3+ (GNU General Public License version 3 or later).

## Project status
Activo (2023.noviembre.18)
